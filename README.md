# street-fighter
Street fighter game to promote Ryu �brand image�

## Dependencies

* `webpack`
* babel

## Install & run

* Clone repo

* `npm i`

* `npm start`

###Start development webpack server:

` npm start`

###Build to production

`npm run build`

Build will fall into dist folder


Webpack and babel settings can be changed in `babel.config.js` and `webpack.config.js`

###Documentation

For more (not really a lot but still more) check out wiki
