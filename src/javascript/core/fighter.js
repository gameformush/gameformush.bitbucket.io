class Fighter {
  name;
  _health;
  attack;
  defense;

  static minCrit = 1;
  static maxCrit = 2;

  constructor({name = "", health = 0, attack = 0, defense = 0}) {
    this.name = name;
    this._health = health;
    this.attack = attack;
    this.defense = defense;
  }

  get health() {
    return this._health;
  }

  set health(value) {
    if (value < 0) {
      this._health = 0;
      return;
    }
    this._health = value;
  }

  getHitPower() {
    return this._critWith(this.attack);
  }

  getBlockPower() {
    return this._critWith(this.defense);
  }

  _critWith(value) {
    return value * this._chance(Fighter.minCrit, Fighter.maxCrit);
  }

  _chance(min, max) {
    return Math.random() * (max - min) + min;
  }
}

export default Fighter;