import EventObserver from '../../helpers/observer';

class IntervalBot extends EventObserver {
  intervalID;

  constructor(interval = 200) {
    super();

    this.interval = interval;
  }

  start() {
    this.intervalID = setInterval(this._fight.bind(this), this.interval);
  }

  _fight() {
    this.broadcast({});
  }

  stop() {
    clearInterval(this.intervalID);
  }
}

export default IntervalBot;