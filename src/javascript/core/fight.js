import EventObserver from "../helpers/observer";

class Fight {
  fighters;
  observer;
  fightEnd;
  winner;

  constructor(fighters = []) {
    this.fighters = fighters;

    this.observer = new EventObserver();

    return new Proxy(this, {
      set(target, name, value) {
        target[name] = value;
        target.notify();
        return true;
      }
    });
  }

  start() {
    if (this.fightEnd) return;

    this.fightEnd = false;
    this.winner = null;
  }

  getObserver() {
    return this.observer;
  }

  hit(attacker, defender) {
    let demage = attacker.getHitPower() - defender.getBlockPower();

    if (demage >= 0) {
      defender.health -= demage;
    }

    if (this._checkForEnd()) {
      this.fightEnd = true;
      this.winner = this.fighters.find(({health}) => health > 0);
    }
  }

  _checkForEnd() {
    return this.fighters.some(({health}) => health === 0);
  }

  takeAction(action) {
    if (this.fightEnd) return;
    action.execute(this);
    this.notify();
  }

  notify() {
    this.observer.broadcast({
      fighters: this.fighters,
      fightEnd: this.fightEnd,
      winner: this.winner
    });
  }
}



export default Fight;