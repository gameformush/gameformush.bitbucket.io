class HitAction {
  attaker;
  defender;

  constructor(attaker, defender) {
    this.attaker = attaker;
    this.defender = defender;
  }

  execute(fight) {
    fight.hit(this.attaker, this.defender);
  }
}

export default HitAction;