import View from './view';
import empty from '../helpers/emptyNode';

class ChoosenFighters extends View {
  element;

  constructor(fighters, observer) {
    super();
 
    this._create(fighters, observer);
  }


  _create(fighters, observer) {
    const wrapper = this.createElement({tagName: 'div', className: 'choosen-fighters'}),
          header = this.createElement({tagName: 'p', className: 'title'}),
          choosen = this.createElement({tagName: 'div'});


    this.renderFighters(fighters, choosen);


    observer.subscribe(() => this.renderFighters(fighters, choosen));

    wrapper.append(header);
    wrapper.append(choosen);
    this.element = wrapper;
  }

  renderFighters(fighters, node) {
    empty(node);

    fighters.forEach(({name}, index) => {
      const fighterName = this.createElement({tagName: 'p', className: 'player-fighter'});
      fighterName.innerHTML = `Player ${index + 1} choose fighter ${name}`;
      node.append(fighterName);
    })
  }
}

export default ChoosenFighters;