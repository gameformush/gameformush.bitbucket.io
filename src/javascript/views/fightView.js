import View from './view';
import FighterView from './fighterView';
import empty from '../helpers/emptyNode';
import EventObserver from '../helpers/observer';

const EVENTS = {
  HIT: 1,
  END: 2
}

class FightView extends View {
  element;
  fightObserver;
  details;
  myObserver = new EventObserver();
  end = false;

  constructor(details, fightObserver) {
    super();

    this.fightObserver = fightObserver;
    this.details = details;

    this._create();
  }

  getObserver() {
    return this.myObserver;
  }

  _create() {
    const wrapper = this.createElement({tagName: 'div', className: 'fight'});

    wrapper.append(...this._setupArena(this.details));

    this.fightObserver.subscribe(this._endFight.bind(this));

    this.element = wrapper;
  }

  _setupArena() {
    return [
      this._createHealthBars(),
      this._createArena(),
      this._createHitButton()
    ];
  }

  _endFight({fightEnd, winner}) {
    if (fightEnd && winner && !this.end) {
      this.end = true;
      this.fightObserver.unsubscribe(this._endFight.bind(this));
      this.element.append(this._showWinner(winner));
    }
  }

  _showWinner({name: winnerName}) {
    const container = this.createElement({tagName: 'div', className: 'winner'}),
          title = this.createElement({tagName: 'h3'}),
          winnerImg = this._createFighter(this.details.find(({name}) => name === winnerName).source),
          toMainScreen = this.createElement({tagName: 'button'}),
          rowContainer = this.createElement({tagName: 'div', className: "row"});

    toMainScreen.innerHTML = 'Fight again!'
    toMainScreen.addEventListener('click', () => this.myObserver.broadcast({event: EVENTS.END}), true);

    title.innerHTML = `${winnerName} winner!!!`

    rowContainer.append(title);
    rowContainer.append(winnerImg);
    rowContainer.append(toMainScreen);
    container.append(rowContainer);

    return container;
  }

  _createHitButton() {
    const hitButton = this.createElement({tagName: 'button', className: 'fight-hit'});
        hitButton.innerHTML = '&#x1F44A;';
        hitButton.addEventListener('click', () => {
          this.myObserver.broadcast({event: EVENTS.HIT});
        }, true);
    return hitButton;
  }

  _createArena() {
    const arena = this.createElement({tagName: 'div', className: 'game-arena'});

    const fightersImg = this.details
         .map(({source}) => this._createFighter(source));

    fightersImg[1].classList.add('fighter-fliped');

    arena.append(...fightersImg);
    return arena;
  }

  _createHealthBars() {
    const healthBars = this.details.map(({health, name}, index) => {
      const bar = this._createBar(name, 0, health, health, updateBar => {
        this.fightObserver.subscribe(({fighters}) => {
          updateBar(fighters[index].health);
        })
      });

      return bar;
    });

    const barsContainer = this.createElement({tagName: 'div', className: 'game-bars'});

    barsContainer.append(...healthBars);
    return barsContainer;
  }

  /*
   * WHY callback?
   * Short answer: to hide inner implementation of bar behind interface
   * Otherwise clients of bar would have to find progress element by them self
   * but progress element might change in future
   */
  _createBar(title, min, max, value, cb) {
    const wrapper = this.createElement({tagName: 'div', className: 'game-bar'}),
          header = this.createElement({tagName: 'h3'}),
          progress = this.createElement({tagName: 'progress', attributes: {max, min, value: value || min}});


    if (typeof cb === 'function') {
      cb(value => {
        progress.value = value;
      })
    }

    header.innerHTML = title;
    wrapper.append(header);
    wrapper.append(progress);
    return wrapper;
  }

  _createFighter(source) {
    const img = FighterView.prototype.createImage.call(this, source);
    return img;
  }
}

export { FightView, EVENTS };