import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, deleteClick) {
    super();

    this.deleteClick = deleteClick;
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const deleteButton = this.createDelete(fighter);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(deleteButton,imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createDelete({_id}) {
    const deleteFighter = this.createElement({tagName: 'button', className: 'delete-fighter'});
    deleteFighter.innerHTML = "DELETE";
    
    deleteFighter.addEventListener('click', () => {
      this.deleteClick(_id);
    });

    return deleteFighter;
  }
}

export default FighterView;