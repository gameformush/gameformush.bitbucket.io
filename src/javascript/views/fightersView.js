import View from './view';
import FighterView from './fighterView';
import { fighterService } from '../services/fightersService';
import DetailsModal from '../modals/detailsModal';
import EventObserver from '../helpers/observer';
import empty from '../helpers/emptyNode';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.deleteClick = this.deleteFighter.bind(this);
    this.fighters = fighters;
    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.createFighters(this.fighters);
    this.detailsModal = new DetailsModal();
    this.observer = new EventObserver();
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    empty(this.element);
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.deleteClick);
      const wrapper = this.createElement({tagName: 'div', className: "fighter-card"});

      const chooseFighter = this.createElement({tagName: 'button', className: 'choose-fighter'});
      chooseFighter.addEventListener('click', () => this.chooseFighter(fighter), true);
      chooseFighter.innerHTML = "choose";

      wrapper.append(fighterView.element);
      wrapper.append(chooseFighter);

      return wrapper;
    });

    
    this.element.append(...fighterElements);
  }

  getChooseObserver() {
    return this.observer;
  }

  deleteFighter(id) {
    fighterService
      .deleteFighter(id)
      .then(({success}) => {
        if (success) {
          this.fighters = this.fighters.filter(({_id}) => _id !== id);
          this.createFighters(this.fighters);
          this.detailsModal.close();
        }
      })
      .catch(console.error);
  }

  chooseFighter(fighter) {
    this._getFighterDetails(fighter)
        .then(details => {
          this.observer.broadcast({details});
        })
        .catch(console.error);
  }

  handleFighterClick(event, fighter) {
    this.detailsModal.setFighter(this._getFighterDetails(fighter));
    const observerPromise = this.detailsModal.show();

    observerPromise
      .then(observer => observer.subscribe(this._handleDetailsChange.bind(this)))
      .catch(console.error);
  }

  _handleDetailsChange({_id, field, value}) {
    const fighter = this.fightersDetailsMap.get(_id);
    fighter[field] = value;
    fighterService
      .updateFighterDetails(_id, fighter)
      .then(console.log)
      .catch(console.error);
  }

  async _getFighterDetails({_id}) {
    if (this.fightersDetailsMap.has(_id)) {
      return this.fightersDetailsMap.get(_id)
    }

    try {
      const details = await fighterService.getFighterDetails(_id);

      this.fightersDetailsMap.set(_id, details);
      return details;
    } catch(error) {
      throw error;
    }
  }

  get fightersDetailsMap() {
    return fightersDetailsMap;
  }

  set fightersDetailsMap(value) {
    return;
  }
}

export default FightersView;