import View from './view';

class CreateFighter extends View {
  element;
  handler;

  constructor(title, handler) {
    super();

    if (typeof handler === 'function') {
      this.handler = handler;
    } else {
      this.handler = () => {};
    }

    this._create(title);
  }

  _create(title) {
    const button = this.createElement({tagName: 'button', className: 'create-fighter'});
    button.innerHTML = title + ' &#x1F44A;';

    button.addEventListener('click', this.handler, true);

    this.element = button;
  }
}

export default CreateFighter;