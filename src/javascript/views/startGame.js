import View from './view';

class StartGame extends View {
  element;
  handler;

  constructor(title, handler) {
    super();

    if (typeof handler === 'function') {
      this.handler = handler;
    } else {
      this.handler = () => {};
    }

    this._create(title);
  }

  _create(title) {
    const startButton = this.createElement({tagName: 'button', className: 'start'});
    startButton.innerHTML = title + ' &#x1F44A;';

    startButton.addEventListener('click', this.handler, true);

    this.element = startButton;
  }
}

export default StartGame;