/**
 * Link object with observer
 * And broadcast on any object setter
 * @param{Object} obj - object to watch
 * @param{EventObserver} observer
 * @returs{Proxy} proxy that wraps given object
 */
function reactify(obj, observer) {
    const handlers = {
        set: function(target, key, value, _) {
            target[key] = value;
            observer.broadcast({key, value});
            return true;
        }
    }

    return new Proxy(obj, handlers);
}

export { reactify };
