/**
 * Class for Observer pattern
 * @class 
 */
class EventObserver {
  /**
   * @constructor
   * @constructs EventObserver
   */ 
  constructor () {
    this.observers = []
  }


  /**
   * Subcribe to observer broadcast
   * @param{function} fn - function will be called during broadcast
   */
  subscribe (fn) {
    this.observers.push(fn)
  }

  /**
   * Remove subscription from observer
   * @param{function} fn - function previously subscribed
   */
  unsubscribe (fn) {
    this.observers = this.observers.filter(subscriber => subscriber !== fn)
  }

  /**
   * Remove subscription from observer
   * @param{Object} data - object that will be passed as subscription functions argument
   */
  broadcast (data) {
    this.observers.forEach(subscriber => subscriber(data))
  }
}

export default EventObserver;
