const API_URL = 'https://street-fighter-express.herokuapp.com/fighter';


/**
 * Helper function to make api calls
 * @param {string} endpoind - api endpoint to call
 * @param {string} method - which HTTP method to use
 * @returns {Promise} json convertion promise
 */
function callApi(endpoind, method, body = {}) {
  const url = API_URL + endpoind;
  const options = {
    method
  };

  if (['PUT', 'POST'].includes(method)) {
    options.headers = {'Content-Type': 'application/json'};
    options.body = JSON.stringify(body);
  }

  return fetch(url, options)
    .then(response =>
        response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }