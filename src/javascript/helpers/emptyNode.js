/**
 * Helper function to clear all DOMNode childern
 * @param {Node} node - dom element
 */
function empty(node) {
  let child = node.lastElementChild;  
    while (child) { 
      node.removeChild(child); 
      child = node.lastElementChild; 
    }
}

export default empty;