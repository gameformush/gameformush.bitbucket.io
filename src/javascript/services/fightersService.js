import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    return await this.callHelper('/', 'GET');
  }

  async getFighterDetails(_id) {
    return await this.callHelper(`/${_id}`, 'GET');
  }

  async updateFighterDetails(_id, {health, attack, defense}) {
    return await this.callHelper(`/${_id}`, 'PUT', {health, attack, defense});
  }

  async deleteFighter(_id) {
    return await this.callHelper(`/${_id}`, 'DELETE');
  }

  async createFighter(fighter) {
    return await this.callHelper('/', 'POST', fighter);
  }

  async callHelper(endpoint, method, body) {
    try {
      return await callApi(endpoint, method, body);
    } catch(error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
