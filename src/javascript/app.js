import FightersView from './views/fightersView';
import StartGame from './views/startGame';
import CreateFighter from './views/createFighter';
import ChoosenFighters from './views/choosenFighters';
import {FightView, EVENTS} from './views/fightView';
import View from './views/view';
import CreateFighterModal from './modals/createFighter';

import { fighterService } from './services/fightersService';

import { reactify } from './helpers/reactify';
import EventObserver from './helpers/observer';
import empty from './helpers/emptyNode';

import Fighter from './core/fighter';
import Fight from './core/fight';
import HitAction from './core/actions/hit';
import IntervalBot from './core/ai/intervalBot';


/**
 * Creates a new App.
 * @class
 * @classdesc App class responsible for DOM manipulations, fighters fetching and starting fight
 */
class App {
  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  static MAX_FIGHTERS = 2;

  /** @public */
  fcObserver = new EventObserver();
  /** @public */
  fightersChoosen = reactify([], this.fcObserver);
  /** @public */
  currentTurn = 0;


  /**
   * @constructor
   */
  constructor() {
    this.createFighterModal = new CreateFighterModal();

    this.startApp();
  }

  /**
   * Fetch fighters and setup root element
   * Handle fecting fail
   */
  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters(),
            fightersView = new FightersView(fighters),
            fightersElement = fightersView.element,
            chooseObserver = fightersView.getChooseObserver();

      chooseObserver.subscribe(({ details }) => {
        this.choose( details );
      })

      const buttons = new View().createElement({tagName: 'div', className:'buttons'});
      const startGame = new StartGame('Fight!', this.startFight.bind(this)).element;
      const createFighter = new CreateFighter('create fighter', this.createFighter.bind(this)).element;

      const choosen = new ChoosenFighters(this.fightersChoosen, this.fcObserver).element;

      buttons.appendChild(createFighter);
      buttons.appendChild(startGame);
      App.rootElement.appendChild(buttons);
      App.rootElement.appendChild(choosen);
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }


  createFighter() {
    this.createFighterModal
      .show()
      .then(observer => {
        observer.subscribe(fighter => {
          fighterService
            .createFighter(fighter)
            .then(() => {
              empty(App.rootElement);
              this.startApp().then(console.log);
            })
            .catch(console.error);
        })
      })
      .catch(console.error);

  }

  /**
   * Add details of fighter to choosen set
   * @param {Object} fighter details - name, health, attack, defence, source
   */
  choose(details) {
    this.fightersChoosen[this.currentTurn++ % App.MAX_FIGHTERS] = details;
  }

  /**
   * Check if players select their fighters and setup Fight, Fighter entities
   * Run FighterView.
   * React to user and bot actions
   */
  startFight() {
    if (this.fightersChoosen.length !== 2) return;

    const fighter1 = new Fighter(this.fightersChoosen[0]),
          fighter2 = new Fighter(this.fightersChoosen[1]);

    const fightObserver = new EventObserver(),
          fight = new Fight([fighter1, fighter2]);

    const bot = new IntervalBot();

    bot.subscribe(() => {
      fight.takeAction(new HitAction(fighter2, fighter1));
    });

    bot.start()

    const fightView = new FightView(this.fightersChoosen, fight.getObserver()),
          fightViewObs = fightView.getObserver();

    fightViewObs.subscribe(({event}) => {
      // Maybe state machine would be better
      switch (event) {
        case EVENTS.HIT: {
          fight.takeAction(new HitAction(fighter1, fighter2));
          break;
        }
        case EVENTS.END: {
          empty(App.rootElement);
          bot.stop();
          this.startApp();
          break;
        }
      }
    })

    fight.getObserver().subscribe(({fightEnd}) => {
      if (fightEnd) {
        bot.stop();
      }
    })

    fight.start();

    empty(App.rootElement);
    App.rootElement.append(fightView.element);
  }
}

export default App;