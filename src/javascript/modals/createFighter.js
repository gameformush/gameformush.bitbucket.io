import View from '../views/view';
import FighterView from "../views/fighterView";
import EventObserver from '../helpers/observer';

/**
 * Fighter create modal window
 * Handle user input and broadcast changes
 * @class
 */
class CreateFighterModal extends View {

  static modalContainer = document.getElementById("create-modal");
  static fields = [
    {name: 'name', type: 'text'},
    {name: 'health', type: 'number'},
    {name: 'attack', type: 'number'},
    {name: 'defense', type: 'number'},
    {name: 'source', type: 'text'}];

  rootElement;
  createPromise;

  /**
   * @constructor
   */
  constructor() {
    super();

    this.store = {};
    this.observer = new EventObserver();
  }


  /**
   * show modal window
   * @public
   */
  async show() {
     this._displayModal();
     return this.observer;
  }


  /**
   * create modal elements and append them to root element
   * @private
   */
  _displayModal() {
    if (this.rootElement) {
      CreateFighterModal.modalContainer.removeChild(this.rootElement);
      this.rootElement = null;
    }

    const closeButton = this.createElement({tagName: "button", className: "close"});
    closeButton.innerHTML = "Close";
    closeButton.addEventListener('click', this.close.bind(this), false);

    const createButton = this.createElement({tagName: 'button'});
    createButton.innerHTML = 'Create';
    createButton.addEventListener('click', this.saveFighter.bind(this));

    CreateFighterModal.modalContainer.style.visibility = "visible";
    this.rootElement = this.createElement({tagName: "div", className: "modal"});
    CreateFighterModal.modalContainer.append(this.rootElement);

    this.rootElement.append(this._createcreate(this.store));
    this.rootElement.append(closeButton);
    this.rootElement.append(createButton);
  }


  saveFighter() {
    this.observer.broadcast(this.store);
  }

  /**
   * Construct create elemnets from fighter create 
   * @private
   * @param{Object} create - fighter create
   * @returns{Node}
   */
  _createcreate(store) {
    let createHolder = this.createElement({tagName: 'div', className: "create-wrapper"});

    let nameElement = this.createElement({tagName: 'span'})

    createHolder.append(nameElement);
    
    CreateFighterModal.fields.forEach(({name, type}) => {
        createHolder.append(this._createField(name, '', type ,(e) => {
           store[name] = e.target.value;
        }));
    });

    return createHolder;
  }


  /**
   * Create field input and lister assign onChange callback to listen changes
   * @private
   * @param{string} name - fighter name
   * @param{string} value - init value of input
   * @param{string} type - type of input
   * @default text
   * @param{function} onChange - onchange callback 
   */
  _createField(name, value = '',type = "text", onChange) {
    let group = this.createElement({tagName: "div", className: "input-group"}),
        label = this.createElement({tagName: "label"}),
        input = this.createElement({tagName: "input", attributes: {type: type, value: value}});

    input.addEventListener('keyup', onChange);
    input.addEventListener('mouseup', onChange);

    label.innerHTML = name;
    label.append(input);
    group.append(label);

    return group;
  }

  /**
   * close modal by removing root element
   * @public
   */ 
  close() {
    CreateFighterModal.modalContainer.style.visibility = "hidden";
    CreateFighterModal.modalContainer.removeChild(this.rootElement);
    this.rootElement = null;
  }
}

export default CreateFighterModal