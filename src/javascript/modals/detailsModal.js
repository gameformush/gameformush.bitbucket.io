import View from '../views/view';
import FighterView from "../views/fighterView";
import EventObserver from '../helpers/observer';

/**
 * Fighter details modal window
 * Handle user input and broadcast changes
 * @class
 */
class DetailsModal extends View {

  static modalContainer = document.getElementById("details-modal");
  static fields = ['health', 'attack', 'defense'];
  rootElement;
  detailsPromise;

  /**
   * @constructor
   */
  constructor() {
    super();

    this.observer = new EventObserver();
  }


  /**
   * Subcribe to observer broadcast
   * @public
   * @param{Promise} detailsPromise - promise with fighter details
   */
  setFighter(detailsPromise) {
    this.detailsPromise = detailsPromise;
  }


  /**
   * show modal window
   * @public
   */
  async show() {
    return this.detailsPromise
        .then(details => {
          this._displayModal(details);
          return this.observer;
        })
        .catch(error => {
          console.error(error);
          DetailsModal.rootElement.innerHTML = "Failed to load";
        });
  }


  /**
   * create modal elements and append them to root element
   * @private
   * @param{Object} details - fighter details
   */
  _displayModal(details) {
    if (this.rootElement) {
      DetailsModal.modalContainer.removeChild(this.rootElement);
      this.rootElement = null;
    }

    let closeButton = this.createElement({tagName: "button", className: "close"});
    closeButton.innerHTML = "Close";
    closeButton.addEventListener('click', this.close.bind(this), false);

    DetailsModal.modalContainer.style.visibility = "visible";
    this.rootElement = this.createElement({tagName: "div", className: "modal"});
    DetailsModal.modalContainer.append(this.rootElement);

    this.rootElement.append(this._createFighter(details));
    this.rootElement.append(this._createDetails(details));
    this.rootElement.append(closeButton);
  }


  /**
   * Construct details elemnets from fighter details 
   * @private
   * @param{Object} details - fighter details
   * @returns{Node}
   */
  _createDetails(details) {
    let detailsHolder = this.createElement({tagName: 'div', className: "details-wrapper"});

    let nameElement = this.createElement({tagName: 'span'})

    detailsHolder.append(nameElement);
    
    DetailsModal.fields.forEach(field => {
        detailsHolder.append(this._createField(field, details[field], 'number' ,(e) => {
           this.observer.broadcast({
            _id: details._id,
            field: field,
            value: +e.target.value
          });
        }));
    });

    return detailsHolder;
  }


  /**
   * Create field input and lister assign onChange callback to listen changes
   * @private
   * @param{string} name - fighter name
   * @param{string} value - init value of input
   * @param{string} type - type of input
   * @default text
   * @param{function} onChange - onchange callback 
   */
  _createField(name, value = '',type = "text", onChange) {
    let group = this.createElement({tagName: "div", className: "input-group"}),
        label = this.createElement({tagName: "label"}),
        input = this.createElement({tagName: "input", attributes: {type: type, value: value}});

    input.addEventListener('keyup', onChange);
    input.addEventListener('mouseup', onChange);

    label.innerHTML = name;
    label.append(input);
    group.append(label);

    return group;
  }


  /**
   * create fighter img based on fighter details
   * @param{Object} details - fighter details
   */
  _createFighter(details) {
    let fighterHolder = this.createElement({tagName: 'div', className: "fighter-wrapper"});
    let fighter = new FighterView(details, () => {}).element;
    fighterHolder.append(fighter);
    return fighterHolder;
  }

  /**
   * close modal by removing root element
   * @public
   */ 
  close() {
    DetailsModal.modalContainer.style.visibility = "hidden";
    DetailsModal.modalContainer.removeChild(this.rootElement);
    this.rootElement = null;
  }
}

export default DetailsModal