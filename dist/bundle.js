/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./src/javascript/app */ "./src/javascript/app.js");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./src/styles/styles.css */ "./src/styles/styles.css");
/* harmony import */ var _src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_src_styles_styles_css__WEBPACK_IMPORTED_MODULE_1__);


new _src_javascript_app__WEBPACK_IMPORTED_MODULE_0__["default"]();

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css":
/*!*********************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js")(false);
// Module
exports.push([module.i, "html,\nbody {\n    height: 100%;\n    width: 100%;\n    margin: 0;\n    padding: 0;\n    background: url('/resources/bg.png');\n    background-size: cover;\n    overflow-x: hidden;\n}\n\nbutton:active {\n    box-shadow: inset 1px 1px 10px black;\n}\n\n#root {\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n    width: 100%;\n}\n\n.fighters {\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    flex: 1;\n    flex-wrap: wrap;\n    padding: 0 15px;\n}\n\n.fighter {\n    display: flex;\n    flex-direction: column;\n    padding: 20px;\n}\n\n.fighter:hover {\n    box-shadow: 0 0 50px 10px rgba(0,0,0,0.06);\n    cursor: pointer;\n}\n\n.name {\n    align-self: center;\n    font-size: 21px;\n    margin-top: 20px;\n}\n\n.fighter-image {\n    height: 260px;\n}\n\n#loading-overlay {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n    font-size: 18px;\n    background: rgba(255, 255, 255, 0.7);\n    visibility: hidden;\n}\n\n.modal-wrapper {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    top: 0;\n    left: 0;\n    visibility: hidden;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    align-content: center;\n    justify-content: center;\n}\n\n.modal {\n    box-shadow: 0 0 10px black;\n    width: 200px;\n    background-color: white;\n    padding: 15px;\n}\n\n\n.fighter-fliped {\n    -moz-transform: scaleX(-1);\n    -o-transform: scaleX(-1);\n    -webkit-transform: scaleX(-1);\n    transform: scaleX(-1);\n    filter: FlipH;\n    -ms-filter: \"FlipH\";\n}\n\n.fighter-card {\n    margin: 10px 10px;\n}\n\n.fighter-card .fighter {\n    border: 2px solid red;\n    background-color: #eee;\n}\n\n.choose-fighter {\n    width: 100%;\n    background-color: #ff0074;\n    border: none;\n    color: white;\n    text-transform: uppercase;\n    padding: 5px;\n    font-weight: bold;\n}\n\n#details-modal {\n    background-color: rgba(0,0,0,.5);\n}\n\n#details-modal .modal {\n    padding: 50px;\n    background-color: #eee;\n    border: 1px solid black;\n}\n#details-modal .modal .close {\n    width: 100%;\n    background-color: #ff0074;\n    border: none;\n    color: white;\n    text-transform: uppercase;\n    padding: 10px 5px;\n    font-weight: bold;\n}\n\n.input-group {\n    margin: 15px 0;\n}\n\n.input-group label {\n    text-transform: capitalize;\n    font-weight: bold;\n}\n\n.input-group input {\n    margin-top: 5px;\n    width: 90%;\n    border: 1px solid #ff0074;\n    padding: 10px;\n}\n\n.choosen-fighters {\n    width: 100%;\n}\n\n.choosen-fighters  > div {\n    padding: 0 20%;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: stretch;\n    align-content: space-around;\n}\n\n.choosen-fighters  > div player-fighter {\n    flex: 1;\n}\n\n.choosen-fighters .player-fighter {\n    background-color: white;\n    padding: 30px;\n    border: 1px solid  #ff0074;\n}\n\n#root .start {\n    font-weight: bold;\n    font-size: 14pt;\n    color: grey;\n    padding: 10px;\n    width: 30%;\n    flex: 2;\n    background-color: #ffe700;\n    border: none;\n    box-shadow: 5px 5px 10px grey;\n}\n\n\n.fight {\n    height: 100vh;\n    width: 100vw;\n    background-color: black;\n    position: relative;\n}\n.fight .game-arena {\n    height: 100%;\n    background-image: url('/resources/arena.gif');\n    background-repeat: no-repeat;\n    background-position: top;\n    display: flex;\n    justify-content: space-around;\n    padding-top: 250px;\n}\n\n.fight .game-arena .fighter-image {\n\n}\n\n.fight .game-bars {\n    width: 100%;\n    position: absolute;\n    display: flex;\n    align-content: space-around;\n    justify-content: space-around;\n    color: white;\n}\n\n.fight .winner {\n    position: absolute;\n    top: 0;\n}\n\n.fight .fight-hit {\n    position: absolute;\n    top: 60%;\n    left: 20%;\n    padding: 20px;\n    font-size: 20pt;\n    background-color: #ff0074;\n    border: none;\n    border-radius: 50%;\n}\n\n.fight .winner {\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    display: flex;\n    flex-direction: row;\n    justify-content: center;\n    background-color: rgba(0,0,0,0.7);\n}\n\n.fight .winner .row {\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n}\n\n.fight .winner .fighter-image {\n    width: 200px;\n}\n\n.fight .winner h3 {\n    color: white;\n    font-weight: bold;\n    text-align: center;\n}\n\n.fight .winner button {\n    width: 200px;\n    font-weight: bold;\n    font-size: 14pt;\n    color: grey;\n    padding: 10px;\n    background-color: #ffe700;\n    border: none;\n    box-shadow: 5px 5px 10px grey;\n}\n\n\n.delete-fighter {\n    color: red;\n    font-weight: bold;\n    background-color: black;\n    border: 1px solid grey;\n}\n\n#details-modal .delete-fighter {\n    display: none;\n}\n\n.create-fighter {\n    font-weight: bold;\n    font-size: 14pt;\n    color: grey;\n    padding: 10px;\n    width: 300px;\n    flex: 2;\n    background-color: #ffe700;\n    border: none;\n    box-shadow: 5px 5px 10px grey;\n}\n\n.buttons {\n    width: 100%;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: stretch;\n    align-content: space-between;\n}\n\n#root .buttons > button {\n    border-left: 1px solid black;\n}", ""]);



/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return '@media ' + item[2] + '{' + content + '}';
      } else {
        return content;
      }
    }).join('');
  }; // import a list of modules into the list


  list.i = function (modules, mediaQuery) {
    if (typeof modules === 'string') {
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    for (var i = 0; i < this.length; i++) {
      var id = this[i][0];

      if (id != null) {
        alreadyImportedModules[id] = true;
      }
    }

    for (i = 0; i < modules.length; i++) {
      var item = modules[i]; // skip already imported module
      // this implementation is not 100% perfect for weird media query combinations
      // when a module is imported multiple times with different media queries.
      // I hope this will never occur (Hey this way we have smaller bundles)

      if (item[0] == null || !alreadyImportedModules[item[0]]) {
        if (mediaQuery && !item[2]) {
          item[2] = mediaQuery;
        } else if (mediaQuery) {
          item[2] = '(' + item[2] + ') and (' + mediaQuery + ')';
        }

        list.push(item);
      }
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || '';
  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */';
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;
  return '/*# ' + data + ' */';
}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/javascript/app.js":
/*!*******************************!*\
  !*** ./src/javascript/app.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_fightersView__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./views/fightersView */ "./src/javascript/views/fightersView.js");
/* harmony import */ var _views_startGame__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./views/startGame */ "./src/javascript/views/startGame.js");
/* harmony import */ var _views_createFighter__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./views/createFighter */ "./src/javascript/views/createFighter.js");
/* harmony import */ var _views_choosenFighters__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./views/choosenFighters */ "./src/javascript/views/choosenFighters.js");
/* harmony import */ var _views_fightView__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./views/fightView */ "./src/javascript/views/fightView.js");
/* harmony import */ var _views_view__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./views/view */ "./src/javascript/views/view.js");
/* harmony import */ var _modals_createFighter__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/createFighter */ "./src/javascript/modals/createFighter.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _helpers_reactify__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./helpers/reactify */ "./src/javascript/helpers/reactify.js");
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./helpers/observer */ "./src/javascript/helpers/observer.js");
/* harmony import */ var _helpers_emptyNode__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./helpers/emptyNode */ "./src/javascript/helpers/emptyNode.js");
/* harmony import */ var _core_fighter__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./core/fighter */ "./src/javascript/core/fighter.js");
/* harmony import */ var _core_fight__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./core/fight */ "./src/javascript/core/fight.js");
/* harmony import */ var _core_actions_hit__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./core/actions/hit */ "./src/javascript/core/actions/hit.js");
/* harmony import */ var _core_ai_intervalBot__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./core/ai/intervalBot */ "./src/javascript/core/ai/intervalBot.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
















/**
 * Creates a new App.
 * @class
 * @classdesc App class responsible for DOM manipulations, fighters fetching and starting fight
 */

class App {
  /** @public */

  /** @public */

  /** @public */

  /**
   * @constructor
   */
  constructor() {
    _defineProperty(this, "fcObserver", new _helpers_observer__WEBPACK_IMPORTED_MODULE_9__["default"]());

    _defineProperty(this, "fightersChoosen", Object(_helpers_reactify__WEBPACK_IMPORTED_MODULE_8__["reactify"])([], this.fcObserver));

    _defineProperty(this, "currentTurn", 0);

    this.createFighterModal = new _modals_createFighter__WEBPACK_IMPORTED_MODULE_6__["default"]();
    this.startApp();
  }
  /**
   * Fetch fighters and setup root element
   * Handle fecting fail
   */


  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      const fighters = await _services_fightersService__WEBPACK_IMPORTED_MODULE_7__["fighterService"].getFighters(),
            fightersView = new _views_fightersView__WEBPACK_IMPORTED_MODULE_0__["default"](fighters),
            fightersElement = fightersView.element,
            chooseObserver = fightersView.getChooseObserver();
      chooseObserver.subscribe(({
        details
      }) => {
        this.choose(details);
      });
      const buttons = new _views_view__WEBPACK_IMPORTED_MODULE_5__["default"]().createElement({
        tagName: 'div',
        className: 'buttons'
      });
      const startGame = new _views_startGame__WEBPACK_IMPORTED_MODULE_1__["default"]('Fight!', this.startFight.bind(this)).element;
      const createFighter = new _views_createFighter__WEBPACK_IMPORTED_MODULE_2__["default"]('create fighter', this.createFighter.bind(this)).element;
      const choosen = new _views_choosenFighters__WEBPACK_IMPORTED_MODULE_3__["default"](this.fightersChoosen, this.fcObserver).element;
      buttons.appendChild(createFighter);
      buttons.appendChild(startGame);
      App.rootElement.appendChild(buttons);
      App.rootElement.appendChild(choosen);
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  createFighter() {
    this.createFighterModal.show().then(observer => {
      observer.subscribe(fighter => {
        _services_fightersService__WEBPACK_IMPORTED_MODULE_7__["fighterService"].createFighter(fighter).then(() => {
          Object(_helpers_emptyNode__WEBPACK_IMPORTED_MODULE_10__["default"])(App.rootElement);
          this.startApp().then(console.log);
        }).catch(console.error);
      });
    }).catch(console.error);
  }
  /**
   * Add details of fighter to choosen set
   * @param {Object} fighter details - name, health, attack, defence, source
   */


  choose(details) {
    this.fightersChoosen[this.currentTurn++ % App.MAX_FIGHTERS] = details;
  }
  /**
   * Check if players select their fighters and setup Fight, Fighter entities
   * Run FighterView.
   * React to user and bot actions
   */


  startFight() {
    if (this.fightersChoosen.length !== 2) return;
    const fighter1 = new _core_fighter__WEBPACK_IMPORTED_MODULE_11__["default"](this.fightersChoosen[0]),
          fighter2 = new _core_fighter__WEBPACK_IMPORTED_MODULE_11__["default"](this.fightersChoosen[1]);
    const fightObserver = new _helpers_observer__WEBPACK_IMPORTED_MODULE_9__["default"](),
          fight = new _core_fight__WEBPACK_IMPORTED_MODULE_12__["default"]([fighter1, fighter2]);
    const bot = new _core_ai_intervalBot__WEBPACK_IMPORTED_MODULE_14__["default"]();
    bot.subscribe(() => {
      fight.takeAction(new _core_actions_hit__WEBPACK_IMPORTED_MODULE_13__["default"](fighter2, fighter1));
    });
    bot.start();
    const fightView = new _views_fightView__WEBPACK_IMPORTED_MODULE_4__["FightView"](this.fightersChoosen, fight.getObserver()),
          fightViewObs = fightView.getObserver();
    fightViewObs.subscribe(({
      event
    }) => {
      // Maybe state machine would be better
      switch (event) {
        case _views_fightView__WEBPACK_IMPORTED_MODULE_4__["EVENTS"].HIT:
          {
            fight.takeAction(new _core_actions_hit__WEBPACK_IMPORTED_MODULE_13__["default"](fighter1, fighter2));
            break;
          }

        case _views_fightView__WEBPACK_IMPORTED_MODULE_4__["EVENTS"].END:
          {
            Object(_helpers_emptyNode__WEBPACK_IMPORTED_MODULE_10__["default"])(App.rootElement);
            bot.stop();
            this.startApp();
            break;
          }
      }
    });
    fight.getObserver().subscribe(({
      fightEnd
    }) => {
      if (fightEnd) {
        bot.stop();
      }
    });
    fight.start();
    Object(_helpers_emptyNode__WEBPACK_IMPORTED_MODULE_10__["default"])(App.rootElement);
    App.rootElement.append(fightView.element);
  }

}

_defineProperty(App, "rootElement", document.getElementById('root'));

_defineProperty(App, "loadingElement", document.getElementById('loading-overlay'));

_defineProperty(App, "MAX_FIGHTERS", 2);

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/javascript/core/actions/hit.js":
/*!********************************************!*\
  !*** ./src/javascript/core/actions/hit.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class HitAction {
  constructor(attaker, defender) {
    _defineProperty(this, "attaker", void 0);

    _defineProperty(this, "defender", void 0);

    this.attaker = attaker;
    this.defender = defender;
  }

  execute(fight) {
    fight.hit(this.attaker, this.defender);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (HitAction);

/***/ }),

/***/ "./src/javascript/core/ai/intervalBot.js":
/*!***********************************************!*\
  !*** ./src/javascript/core/ai/intervalBot.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../helpers/observer */ "./src/javascript/helpers/observer.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



class IntervalBot extends _helpers_observer__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(interval = 200) {
    super();

    _defineProperty(this, "intervalID", void 0);

    this.interval = interval;
  }

  start() {
    this.intervalID = setInterval(this._fight.bind(this), this.interval);
  }

  _fight() {
    this.broadcast({});
  }

  stop() {
    clearInterval(this.intervalID);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (IntervalBot);

/***/ }),

/***/ "./src/javascript/core/fight.js":
/*!**************************************!*\
  !*** ./src/javascript/core/fight.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/observer */ "./src/javascript/helpers/observer.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



class Fight {
  constructor(fighters = []) {
    _defineProperty(this, "fighters", void 0);

    _defineProperty(this, "observer", void 0);

    _defineProperty(this, "fightEnd", void 0);

    _defineProperty(this, "winner", void 0);

    this.fighters = fighters;
    this.observer = new _helpers_observer__WEBPACK_IMPORTED_MODULE_0__["default"]();
    return new Proxy(this, {
      set(target, name, value) {
        target[name] = value;
        target.notify();
        return true;
      }

    });
  }

  start() {
    if (this.fightEnd) return;
    this.fightEnd = false;
    this.winner = null;
  }

  getObserver() {
    return this.observer;
  }

  hit(attacker, defender) {
    let demage = attacker.getHitPower() - defender.getBlockPower();

    if (demage >= 0) {
      defender.health -= demage;
    }

    if (this._checkForEnd()) {
      this.fightEnd = true;
      this.winner = this.fighters.find(({
        health
      }) => health > 0);
    }
  }

  _checkForEnd() {
    return this.fighters.some(({
      health
    }) => health === 0);
  }

  takeAction(action) {
    if (this.fightEnd) return;
    action.execute(this);
    this.notify();
  }

  notify() {
    this.observer.broadcast({
      fighters: this.fighters,
      fightEnd: this.fightEnd,
      winner: this.winner
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Fight);

/***/ }),

/***/ "./src/javascript/core/fighter.js":
/*!****************************************!*\
  !*** ./src/javascript/core/fighter.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Fighter {
  constructor({
    name = "",
    health = 0,
    attack = 0,
    defense = 0
  }) {
    _defineProperty(this, "name", void 0);

    _defineProperty(this, "_health", void 0);

    _defineProperty(this, "attack", void 0);

    _defineProperty(this, "defense", void 0);

    this.name = name;
    this._health = health;
    this.attack = attack;
    this.defense = defense;
  }

  get health() {
    return this._health;
  }

  set health(value) {
    if (value < 0) {
      this._health = 0;
      return;
    }

    this._health = value;
  }

  getHitPower() {
    return this._critWith(this.attack);
  }

  getBlockPower() {
    return this._critWith(this.defense);
  }

  _critWith(value) {
    return value * this._chance(Fighter.minCrit, Fighter.maxCrit);
  }

  _chance(min, max) {
    return Math.random() * (max - min) + min;
  }

}

_defineProperty(Fighter, "minCrit", 1);

_defineProperty(Fighter, "maxCrit", 2);

/* harmony default export */ __webpack_exports__["default"] = (Fighter);

/***/ }),

/***/ "./src/javascript/helpers/apiHelper.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/apiHelper.js ***!
  \*********************************************/
/*! exports provided: callApi */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "callApi", function() { return callApi; });
const API_URL = 'https://street-fighter-express.herokuapp.com/fighter';
/**
 * Helper function to make api calls
 * @param {string} endpoind - api endpoint to call
 * @param {string} method - which HTTP method to use
 * @returns {Promise} json convertion promise
 */

function callApi(endpoind, method, body = {}) {
  const url = API_URL + endpoind;
  const options = {
    method
  };

  if (['PUT', 'POST'].includes(method)) {
    options.headers = {
      'Content-Type': 'application/json'
    };
    options.body = JSON.stringify(body);
  }

  return fetch(url, options).then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load'))).catch(error => {
    throw error;
  });
}



/***/ }),

/***/ "./src/javascript/helpers/emptyNode.js":
/*!*********************************************!*\
  !*** ./src/javascript/helpers/emptyNode.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Helper function to clear all DOMNode childern
 * @param {Node} node - dom element
 */
function empty(node) {
  let child = node.lastElementChild;

  while (child) {
    node.removeChild(child);
    child = node.lastElementChild;
  }
}

/* harmony default export */ __webpack_exports__["default"] = (empty);

/***/ }),

/***/ "./src/javascript/helpers/observer.js":
/*!********************************************!*\
  !*** ./src/javascript/helpers/observer.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Class for Observer pattern
 * @class 
 */
class EventObserver {
  /**
   * @constructor
   * @constructs EventObserver
   */
  constructor() {
    this.observers = [];
  }
  /**
   * Subcribe to observer broadcast
   * @param{function} fn - function will be called during broadcast
   */


  subscribe(fn) {
    this.observers.push(fn);
  }
  /**
   * Remove subscription from observer
   * @param{function} fn - function previously subscribed
   */


  unsubscribe(fn) {
    this.observers = this.observers.filter(subscriber => subscriber !== fn);
  }
  /**
   * Remove subscription from observer
   * @param{Object} data - object that will be passed as subscription functions argument
   */


  broadcast(data) {
    this.observers.forEach(subscriber => subscriber(data));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (EventObserver);

/***/ }),

/***/ "./src/javascript/helpers/reactify.js":
/*!********************************************!*\
  !*** ./src/javascript/helpers/reactify.js ***!
  \********************************************/
/*! exports provided: reactify */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reactify", function() { return reactify; });
/**
 * Link object with observer
 * And broadcast on any object setter
 * @param{Object} obj - object to watch
 * @param{EventObserver} observer
 * @returs{Proxy} proxy that wraps given object
 */
function reactify(obj, observer) {
  const handlers = {
    set: function (target, key, value, _) {
      target[key] = value;
      observer.broadcast({
        key,
        value
      });
      return true;
    }
  };
  return new Proxy(obj, handlers);
}



/***/ }),

/***/ "./src/javascript/modals/createFighter.js":
/*!************************************************!*\
  !*** ./src/javascript/modals/createFighter.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/view */ "./src/javascript/views/view.js");
/* harmony import */ var _views_fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../views/fighterView */ "./src/javascript/views/fighterView.js");
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/observer */ "./src/javascript/helpers/observer.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/**
 * Fighter create modal window
 * Handle user input and broadcast changes
 * @class
 */

class CreateFighterModal extends _views_view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  /**
   * @constructor
   */
  constructor() {
    super();

    _defineProperty(this, "rootElement", void 0);

    _defineProperty(this, "createPromise", void 0);

    this.store = {};
    this.observer = new _helpers_observer__WEBPACK_IMPORTED_MODULE_2__["default"]();
  }
  /**
   * show modal window
   * @public
   */


  async show() {
    this._displayModal();

    return this.observer;
  }
  /**
   * create modal elements and append them to root element
   * @private
   */


  _displayModal() {
    if (this.rootElement) {
      CreateFighterModal.modalContainer.removeChild(this.rootElement);
      this.rootElement = null;
    }

    const closeButton = this.createElement({
      tagName: "button",
      className: "close"
    });
    closeButton.innerHTML = "Close";
    closeButton.addEventListener('click', this.close.bind(this), false);
    const createButton = this.createElement({
      tagName: 'button'
    });
    createButton.innerHTML = 'Create';
    createButton.addEventListener('click', this.saveFighter.bind(this));
    CreateFighterModal.modalContainer.style.visibility = "visible";
    this.rootElement = this.createElement({
      tagName: "div",
      className: "modal"
    });
    CreateFighterModal.modalContainer.append(this.rootElement);
    this.rootElement.append(this._createcreate(this.store));
    this.rootElement.append(closeButton);
    this.rootElement.append(createButton);
  }

  saveFighter() {
    this.observer.broadcast(this.store);
  }
  /**
   * Construct create elemnets from fighter create 
   * @private
   * @param{Object} create - fighter create
   * @returns{Node}
   */


  _createcreate(store) {
    let createHolder = this.createElement({
      tagName: 'div',
      className: "create-wrapper"
    });
    let nameElement = this.createElement({
      tagName: 'span'
    });
    createHolder.append(nameElement);
    CreateFighterModal.fields.forEach(({
      name,
      type
    }) => {
      createHolder.append(this._createField(name, '', type, e => {
        store[name] = e.target.value;
      }));
    });
    return createHolder;
  }
  /**
   * Create field input and lister assign onChange callback to listen changes
   * @private
   * @param{string} name - fighter name
   * @param{string} value - init value of input
   * @param{string} type - type of input
   * @default text
   * @param{function} onChange - onchange callback 
   */


  _createField(name, value = '', type = "text", onChange) {
    let group = this.createElement({
      tagName: "div",
      className: "input-group"
    }),
        label = this.createElement({
      tagName: "label"
    }),
        input = this.createElement({
      tagName: "input",
      attributes: {
        type: type,
        value: value
      }
    });
    input.addEventListener('keyup', onChange);
    input.addEventListener('mouseup', onChange);
    label.innerHTML = name;
    label.append(input);
    group.append(label);
    return group;
  }
  /**
   * close modal by removing root element
   * @public
   */


  close() {
    CreateFighterModal.modalContainer.style.visibility = "hidden";
    CreateFighterModal.modalContainer.removeChild(this.rootElement);
    this.rootElement = null;
  }

}

_defineProperty(CreateFighterModal, "modalContainer", document.getElementById("create-modal"));

_defineProperty(CreateFighterModal, "fields", [{
  name: 'name',
  type: 'text'
}, {
  name: 'health',
  type: 'number'
}, {
  name: 'attack',
  type: 'number'
}, {
  name: 'defense',
  type: 'number'
}, {
  name: 'source',
  type: 'text'
}]);

/* harmony default export */ __webpack_exports__["default"] = (CreateFighterModal);

/***/ }),

/***/ "./src/javascript/modals/detailsModal.js":
/*!***********************************************!*\
  !*** ./src/javascript/modals/detailsModal.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _views_view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../views/view */ "./src/javascript/views/view.js");
/* harmony import */ var _views_fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../views/fighterView */ "./src/javascript/views/fighterView.js");
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/observer */ "./src/javascript/helpers/observer.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/**
 * Fighter details modal window
 * Handle user input and broadcast changes
 * @class
 */

class DetailsModal extends _views_view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  /**
   * @constructor
   */
  constructor() {
    super();

    _defineProperty(this, "rootElement", void 0);

    _defineProperty(this, "detailsPromise", void 0);

    this.observer = new _helpers_observer__WEBPACK_IMPORTED_MODULE_2__["default"]();
  }
  /**
   * Subcribe to observer broadcast
   * @public
   * @param{Promise} detailsPromise - promise with fighter details
   */


  setFighter(detailsPromise) {
    this.detailsPromise = detailsPromise;
  }
  /**
   * show modal window
   * @public
   */


  async show() {
    return this.detailsPromise.then(details => {
      this._displayModal(details);

      return this.observer;
    }).catch(error => {
      console.error(error);
      DetailsModal.rootElement.innerHTML = "Failed to load";
    });
  }
  /**
   * create modal elements and append them to root element
   * @private
   * @param{Object} details - fighter details
   */


  _displayModal(details) {
    if (this.rootElement) {
      DetailsModal.modalContainer.removeChild(this.rootElement);
      this.rootElement = null;
    }

    let closeButton = this.createElement({
      tagName: "button",
      className: "close"
    });
    closeButton.innerHTML = "Close";
    closeButton.addEventListener('click', this.close.bind(this), false);
    DetailsModal.modalContainer.style.visibility = "visible";
    this.rootElement = this.createElement({
      tagName: "div",
      className: "modal"
    });
    DetailsModal.modalContainer.append(this.rootElement);
    this.rootElement.append(this._createFighter(details));
    this.rootElement.append(this._createDetails(details));
    this.rootElement.append(closeButton);
  }
  /**
   * Construct details elemnets from fighter details 
   * @private
   * @param{Object} details - fighter details
   * @returns{Node}
   */


  _createDetails(details) {
    let detailsHolder = this.createElement({
      tagName: 'div',
      className: "details-wrapper"
    });
    let nameElement = this.createElement({
      tagName: 'span'
    });
    detailsHolder.append(nameElement);
    DetailsModal.fields.forEach(field => {
      detailsHolder.append(this._createField(field, details[field], 'number', e => {
        this.observer.broadcast({
          _id: details._id,
          field: field,
          value: +e.target.value
        });
      }));
    });
    return detailsHolder;
  }
  /**
   * Create field input and lister assign onChange callback to listen changes
   * @private
   * @param{string} name - fighter name
   * @param{string} value - init value of input
   * @param{string} type - type of input
   * @default text
   * @param{function} onChange - onchange callback 
   */


  _createField(name, value = '', type = "text", onChange) {
    let group = this.createElement({
      tagName: "div",
      className: "input-group"
    }),
        label = this.createElement({
      tagName: "label"
    }),
        input = this.createElement({
      tagName: "input",
      attributes: {
        type: type,
        value: value
      }
    });
    input.addEventListener('keyup', onChange);
    input.addEventListener('mouseup', onChange);
    label.innerHTML = name;
    label.append(input);
    group.append(label);
    return group;
  }
  /**
   * create fighter img based on fighter details
   * @param{Object} details - fighter details
   */


  _createFighter(details) {
    let fighterHolder = this.createElement({
      tagName: 'div',
      className: "fighter-wrapper"
    });
    let fighter = new _views_fighterView__WEBPACK_IMPORTED_MODULE_1__["default"](details, () => {}).element;
    fighterHolder.append(fighter);
    return fighterHolder;
  }
  /**
   * close modal by removing root element
   * @public
   */


  close() {
    DetailsModal.modalContainer.style.visibility = "hidden";
    DetailsModal.modalContainer.removeChild(this.rootElement);
    this.rootElement = null;
  }

}

_defineProperty(DetailsModal, "modalContainer", document.getElementById("details-modal"));

_defineProperty(DetailsModal, "fields", ['health', 'attack', 'defense']);

/* harmony default export */ __webpack_exports__["default"] = (DetailsModal);

/***/ }),

/***/ "./src/javascript/services/fightersService.js":
/*!****************************************************!*\
  !*** ./src/javascript/services/fightersService.js ***!
  \****************************************************/
/*! exports provided: fighterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fighterService", function() { return fighterService; });
/* harmony import */ var _helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/apiHelper */ "./src/javascript/helpers/apiHelper.js");


class FighterService {
  async getFighters() {
    return await this.callHelper('/', 'GET');
  }

  async getFighterDetails(_id) {
    return await this.callHelper(`/${_id}`, 'GET');
  }

  async updateFighterDetails(_id, {
    health,
    attack,
    defense
  }) {
    return await this.callHelper(`/${_id}`, 'PUT', {
      health,
      attack,
      defense
    });
  }

  async deleteFighter(_id) {
    return await this.callHelper(`/${_id}`, 'DELETE');
  }

  async createFighter(fighter) {
    return await this.callHelper('/', 'POST', fighter);
  }

  async callHelper(endpoint, method, body) {
    try {
      return await Object(_helpers_apiHelper__WEBPACK_IMPORTED_MODULE_0__["callApi"])(endpoint, method, body);
    } catch (error) {
      throw error;
    }
  }

}

const fighterService = new FighterService();

/***/ }),

/***/ "./src/javascript/views/choosenFighters.js":
/*!*************************************************!*\
  !*** ./src/javascript/views/choosenFighters.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
/* harmony import */ var _helpers_emptyNode__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../helpers/emptyNode */ "./src/javascript/helpers/emptyNode.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




class ChoosenFighters extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters, observer) {
    super();

    _defineProperty(this, "element", void 0);

    this._create(fighters, observer);
  }

  _create(fighters, observer) {
    const wrapper = this.createElement({
      tagName: 'div',
      className: 'choosen-fighters'
    }),
          header = this.createElement({
      tagName: 'p',
      className: 'title'
    }),
          choosen = this.createElement({
      tagName: 'div'
    });
    this.renderFighters(fighters, choosen);
    observer.subscribe(() => this.renderFighters(fighters, choosen));
    wrapper.append(header);
    wrapper.append(choosen);
    this.element = wrapper;
  }

  renderFighters(fighters, node) {
    Object(_helpers_emptyNode__WEBPACK_IMPORTED_MODULE_1__["default"])(node);
    fighters.forEach(({
      name
    }, index) => {
      const fighterName = this.createElement({
        tagName: 'p',
        className: 'player-fighter'
      });
      fighterName.innerHTML = `Player ${index + 1} choose fighter ${name}`;
      node.append(fighterName);
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ChoosenFighters);

/***/ }),

/***/ "./src/javascript/views/createFighter.js":
/*!***********************************************!*\
  !*** ./src/javascript/views/createFighter.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



class CreateFighter extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(title, handler) {
    super();

    _defineProperty(this, "element", void 0);

    _defineProperty(this, "handler", void 0);

    if (typeof handler === 'function') {
      this.handler = handler;
    } else {
      this.handler = () => {};
    }

    this._create(title);
  }

  _create(title) {
    const button = this.createElement({
      tagName: 'button',
      className: 'create-fighter'
    });
    button.innerHTML = title + ' &#x1F44A;';
    button.addEventListener('click', this.handler, true);
    this.element = button;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (CreateFighter);

/***/ }),

/***/ "./src/javascript/views/fightView.js":
/*!*******************************************!*\
  !*** ./src/javascript/views/fightView.js ***!
  \*******************************************/
/*! exports provided: FightView, EVENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FightView", function() { return FightView; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EVENTS", function() { return EVENTS; });
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/views/fighterView.js");
/* harmony import */ var _helpers_emptyNode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/emptyNode */ "./src/javascript/helpers/emptyNode.js");
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/observer */ "./src/javascript/helpers/observer.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





const EVENTS = {
  HIT: 1,
  END: 2
};

class FightView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(details, fightObserver) {
    super();

    _defineProperty(this, "element", void 0);

    _defineProperty(this, "fightObserver", void 0);

    _defineProperty(this, "details", void 0);

    _defineProperty(this, "myObserver", new _helpers_observer__WEBPACK_IMPORTED_MODULE_3__["default"]());

    _defineProperty(this, "end", false);

    this.fightObserver = fightObserver;
    this.details = details;

    this._create();
  }

  getObserver() {
    return this.myObserver;
  }

  _create() {
    const wrapper = this.createElement({
      tagName: 'div',
      className: 'fight'
    });
    wrapper.append(...this._setupArena(this.details));
    this.fightObserver.subscribe(this._endFight.bind(this));
    this.element = wrapper;
  }

  _setupArena() {
    return [this._createHealthBars(), this._createArena(), this._createHitButton()];
  }

  _endFight({
    fightEnd,
    winner
  }) {
    if (fightEnd && winner && !this.end) {
      this.end = true;
      this.fightObserver.unsubscribe(this._endFight.bind(this));
      this.element.append(this._showWinner(winner));
    }
  }

  _showWinner({
    name: winnerName
  }) {
    const container = this.createElement({
      tagName: 'div',
      className: 'winner'
    }),
          title = this.createElement({
      tagName: 'h3'
    }),
          winnerImg = this._createFighter(this.details.find(({
      name
    }) => name === winnerName).source),
          toMainScreen = this.createElement({
      tagName: 'button'
    }),
          rowContainer = this.createElement({
      tagName: 'div',
      className: "row"
    });

    toMainScreen.innerHTML = 'Fight again!';
    toMainScreen.addEventListener('click', () => this.myObserver.broadcast({
      event: EVENTS.END
    }), true);
    title.innerHTML = `${winnerName} winner!!!`;
    rowContainer.append(title);
    rowContainer.append(winnerImg);
    rowContainer.append(toMainScreen);
    container.append(rowContainer);
    return container;
  }

  _createHitButton() {
    const hitButton = this.createElement({
      tagName: 'button',
      className: 'fight-hit'
    });
    hitButton.innerHTML = '&#x1F44A;';
    hitButton.addEventListener('click', () => {
      this.myObserver.broadcast({
        event: EVENTS.HIT
      });
    }, true);
    return hitButton;
  }

  _createArena() {
    const arena = this.createElement({
      tagName: 'div',
      className: 'game-arena'
    });
    const fightersImg = this.details.map(({
      source
    }) => this._createFighter(source));
    fightersImg[1].classList.add('fighter-fliped');
    arena.append(...fightersImg);
    return arena;
  }

  _createHealthBars() {
    const healthBars = this.details.map(({
      health,
      name
    }, index) => {
      const bar = this._createBar(name, 0, health, health, updateBar => {
        this.fightObserver.subscribe(({
          fighters
        }) => {
          updateBar(fighters[index].health);
        });
      });

      return bar;
    });
    const barsContainer = this.createElement({
      tagName: 'div',
      className: 'game-bars'
    });
    barsContainer.append(...healthBars);
    return barsContainer;
  }
  /*
   * WHY callback?
   * Short answer: to hide inner implementation of bar behind interface
   * Otherwise clients of bar would have to find progress element by them self
   * but progress element might change in future
   */


  _createBar(title, min, max, value, cb) {
    const wrapper = this.createElement({
      tagName: 'div',
      className: 'game-bar'
    }),
          header = this.createElement({
      tagName: 'h3'
    }),
          progress = this.createElement({
      tagName: 'progress',
      attributes: {
        max,
        min,
        value: value || min
      }
    });

    if (typeof cb === 'function') {
      cb(value => {
        progress.value = value;
      });
    }

    header.innerHTML = title;
    wrapper.append(header);
    wrapper.append(progress);
    return wrapper;
  }

  _createFighter(source) {
    const img = _fighterView__WEBPACK_IMPORTED_MODULE_1__["default"].prototype.createImage.call(this, source);
    return img;
  }

}



/***/ }),

/***/ "./src/javascript/views/fighterView.js":
/*!*********************************************!*\
  !*** ./src/javascript/views/fighterView.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");


class FighterView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighter, handleClick, deleteClick) {
    super();
    this.deleteClick = deleteClick;
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const {
      name,
      source
    } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const deleteButton = this.createDelete(fighter);
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighter'
    });
    this.element.append(deleteButton, imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({
      tagName: 'span',
      className: 'name'
    });
    nameElement.innerText = name;
    return nameElement;
  }

  createImage(source) {
    const attributes = {
      src: source
    };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
    return imgElement;
  }

  createDelete({
    _id
  }) {
    const deleteFighter = this.createElement({
      tagName: 'button',
      className: 'delete-fighter'
    });
    deleteFighter.innerHTML = "DELETE";
    deleteFighter.addEventListener('click', () => {
      this.deleteClick(_id);
    });
    return deleteFighter;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FighterView);

/***/ }),

/***/ "./src/javascript/views/fightersView.js":
/*!**********************************************!*\
  !*** ./src/javascript/views/fightersView.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
/* harmony import */ var _fighterView__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fighterView */ "./src/javascript/views/fighterView.js");
/* harmony import */ var _services_fightersService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/fightersService */ "./src/javascript/services/fightersService.js");
/* harmony import */ var _modals_detailsModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../modals/detailsModal */ "./src/javascript/modals/detailsModal.js");
/* harmony import */ var _helpers_observer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../helpers/observer */ "./src/javascript/helpers/observer.js");
/* harmony import */ var _helpers_emptyNode__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers/emptyNode */ "./src/javascript/helpers/emptyNode.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








class FightersView extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(fighters) {
    super();

    _defineProperty(this, "fightersDetailsMap", new Map());

    this.handleClick = this.handleFighterClick.bind(this);
    this.deleteClick = this.deleteFighter.bind(this);
    this.fighters = fighters;
    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters'
    });
    this.createFighters(this.fighters);
    this.detailsModal = new _modals_detailsModal__WEBPACK_IMPORTED_MODULE_3__["default"]();
    this.observer = new _helpers_observer__WEBPACK_IMPORTED_MODULE_4__["default"]();
  }

  createFighters(fighters) {
    Object(_helpers_emptyNode__WEBPACK_IMPORTED_MODULE_5__["default"])(this.element);
    const fighterElements = fighters.map(fighter => {
      const fighterView = new _fighterView__WEBPACK_IMPORTED_MODULE_1__["default"](fighter, this.handleClick, this.deleteClick);
      const wrapper = this.createElement({
        tagName: 'div',
        className: "fighter-card"
      });
      const chooseFighter = this.createElement({
        tagName: 'button',
        className: 'choose-fighter'
      });
      chooseFighter.addEventListener('click', () => this.chooseFighter(fighter), true);
      chooseFighter.innerHTML = "choose";
      wrapper.append(fighterView.element);
      wrapper.append(chooseFighter);
      return wrapper;
    });
    this.element.append(...fighterElements);
  }

  getChooseObserver() {
    return this.observer;
  }

  deleteFighter(id) {
    _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].deleteFighter(id).then(({
      success
    }) => {
      if (success) {
        this.fighters = this.fighters.filter(({
          _id
        }) => _id !== id);
        this.createFighters(this.fighters);
        this.detailsModal.close();
      }
    }).catch(console.error);
  }

  chooseFighter(fighter) {
    this._getFighterDetails(fighter).then(details => {
      this.observer.broadcast({
        details
      });
    }).catch(console.error);
  }

  handleFighterClick(event, fighter) {
    this.detailsModal.setFighter(this._getFighterDetails(fighter));
    const observerPromise = this.detailsModal.show();
    observerPromise.then(observer => observer.subscribe(this._handleDetailsChange.bind(this))).catch(console.error);
  }

  _handleDetailsChange({
    _id,
    field,
    value
  }) {
    const fighter = this.fightersDetailsMap.get(_id);
    fighter[field] = value;
    _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].updateFighterDetails(_id, fighter).then(console.log).catch(console.error);
  }

  async _getFighterDetails({
    _id
  }) {
    if (this.fightersDetailsMap.has(_id)) {
      return this.fightersDetailsMap.get(_id);
    }

    try {
      const details = await _services_fightersService__WEBPACK_IMPORTED_MODULE_2__["fighterService"].getFighterDetails(_id);
      this.fightersDetailsMap.set(_id, details);
      return details;
    } catch (error) {
      throw error;
    }
  }

  get fightersDetailsMap() {
    return fightersDetailsMap;
  }

  set fightersDetailsMap(value) {
    return;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FightersView);

/***/ }),

/***/ "./src/javascript/views/startGame.js":
/*!*******************************************!*\
  !*** ./src/javascript/views/startGame.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view */ "./src/javascript/views/view.js");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



class StartGame extends _view__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(title, handler) {
    super();

    _defineProperty(this, "element", void 0);

    _defineProperty(this, "handler", void 0);

    if (typeof handler === 'function') {
      this.handler = handler;
    } else {
      this.handler = () => {};
    }

    this._create(title);
  }

  _create(title) {
    const startButton = this.createElement({
      tagName: 'button',
      className: 'start'
    });
    startButton.innerHTML = title + ' &#x1F44A;';
    startButton.addEventListener('click', this.handler, true);
    this.element = startButton;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (StartGame);

/***/ }),

/***/ "./src/javascript/views/view.js":
/*!**************************************!*\
  !*** ./src/javascript/views/view.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class View {
  constructor() {
    _defineProperty(this, "element", void 0);
  }

  createElement({
    tagName,
    className = '',
    attributes = {}
  }) {
    const element = document.createElement(tagName);

    if (className !== "") {
      element.classList.add(className);
    }

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    return element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (View);

/***/ }),

/***/ "./src/styles/styles.css":
/*!*******************************!*\
  !*** ./src/styles/styles.css ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../node_modules/css-loader/dist/cjs.js!./styles.css */ "./node_modules/css-loader/dist/cjs.js!./src/styles/styles.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map